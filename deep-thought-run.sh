#!/bin/bash

if [ `whoami` != 'root' ]
then
  echo "You must be root to do this."
  exit
fi

function init() {
  rm -rf scans
  echo "Installing prequesites..."
  mkdir scans
  cd scans
  mkdir outputs

  apt-get update
  apt-get install git 
  apt-get install dig 
  apt-get install host 
  apt-get install drill
  apt-get install nslookup
  apt-get install rsync

  git clone --depth 1 https://github.com/CISOfy/lynis
  git clone --depth 1 https://github.com/drwetter/testssl.sh.git
  git clone --depth 1 https://github.com/trimstray/otseca
  git clone --depth 1 https://github.com/Jsitech/JShielder.git

  cd otseca && ./setup.sh install
  echo "Setup complete!"
  exit
}

function run_scan() {
  if [ -z "$1" ]
  then
    echo "Please suppy the scan name. e.g. 1 or first"
    exit
  fi

  echo "Running $1 scan..."
  cd scans

  echo "Running lynis..."
  cd lynis
  ./lynis audit system | tee "../outputs/lynis-console-$1.log"
  mv /var/log/lynis.log "../outputs/lynis-log-$1.log"
  mv /var/log/lynis-report.dat "../outputs/lynis-report-$1.dat"

  echo "Running testssl..."
  cd ../testssl.sh
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 localhost
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost

  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t ftp localhost:21
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:21

  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t smtp localhost:465
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t smtp localhost:587
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:465
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:587

  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t pop3 localhost:110
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t pop3 localhost:995
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:110
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:995

  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t imap localhost:993
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:993

  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t postgres localhost:5432
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:5432

  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 -t mysql localhost:3306
  ./testssl.sh --logfile "../outputs/testssl-$1.log" --append --connect-timeout 10 --openssl-timeout 10 --ssl-native localhost:3306
  echo "testssl done!"

  echo "Running otseca..."
  cd ../otseca
  otseca --ignore-failed --tasks system,kernel,permissions,services,network,distro,external | tee "otseca-$1.log"
  mv data/output "../outputs/otseca-$1"
  echo "All scans done!"
}

function open_jshielder() {
  cd scans/JShielder
  ./jshielder.sh
}

function tar_files() {
  if id "ftpuser" &>/dev/null
  then
    echo "ftpuser already exists!"
  else
    echo "ftpuser doesn't exist, creating it..."
    adduser ftpuser
  fi
  cd scans
  tar cfvz "scans-output-$(date +%Y%m%d-%H%M%S).tar.gz" outputs/
  cp *.tar.gz /home/ftpuser
  echo "Moved archived files to /home/ftpuser"
  echo "To download the files use an ftp server and the ftpuser along with your password."
}

if [ -z "$1" ]
  then
    echo ""
    echo "Init the repo with 'init'"
    echo "Run a test with 'run_scan your-name'"
    echo "Open JShielder with 'open_jshielder' to run tasks 3, 8, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 30 and 31 select system first and the option 6 (custom)"
    echo "Create an export with 'tar_files'"
    echo ""
fi

$1 $2 # runs a funcion like init or run_scan

echo "Finished!"