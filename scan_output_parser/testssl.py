import re


def testssl_parse(path):
    with open(path) as handle:
        text = handle.read()

    testssl_overall = {
        "443": {"open": False, "ssl": False},
        "21": {"open": False, "ssl": False},
        "465": {"open": False, "ssl": False},
        "587": {"open": False, "ssl": False},
        "110": {"open": False, "ssl": False},
        "995": {"open": False, "ssl": False},
        "993": {"open": False, "ssl": False},
        "5432": {"open": False, "ssl": False},
        "3306": {"open": False, "ssl": False}
    }

    tests = text.split("## Scan started as: ")
    for test in tests:
        port = re.findall("Start .*? -->> 127\.0\.0\.1:(\d*) \(localhost\) <<--", test)
        if not port:
            continue
        port = port[0]
        if "Overall Grade" in test:
            testssl_overall[port]["ssl"] = True
            testssl_overall[port]["open"] = True
        if "firewall" not in test:
            testssl_overall[port]["open"] = True
            continue
    return testssl_overall
