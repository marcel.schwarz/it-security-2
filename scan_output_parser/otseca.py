import os.path
import re

OTSECA_FILES = {
    "distro": "distro.all.log.html",
    "kernel": "kernel.all.log.html",
    "network": "network.all.log.html",
    "permission": "permissions.all.log.html",
    "services": "services.all.log.html",
    "system": "system.all.log.html"
}


def otseca_box_counts(path_to_report):
    counts = {}
    for (name, curr_file) in OTSECA_FILES.items():
        curr_path = os.path.join(path_to_report, curr_file)
        with open(curr_path, encoding="UTF-8") as handle:
            text = handle.read()
        counts[name] = {
            "green": (len(re.findall("background-color: #1F9D55", text))),
            "yellow": (len(re.findall("background-color: #F2D024", text))),
            "red": (len(re.findall("background-color: #CC1F1A", text))),
            "total": (len(re.findall("background-color:", text)))
        }
    return counts


def otseca_distro_info(path_to_report):
    with open(os.path.join(path_to_report, OTSECA_FILES["distro"])) as handle:
        text = handle.read()

    pkg_count = len(re.findall("ii {2}", text))
    upgrades_count = re.findall(r"(\d+) .*? (\d+) .*? (\d+) .*? (\d+) .*", text).pop()

    return {
        "pkgCount": pkg_count,
        "upgraded": int(upgrades_count[0]),
        "newlyInstalled": int(upgrades_count[1]),
        "remove": int(upgrades_count[2]),
        "notUpgraded": int(upgrades_count[3])
    }


def otseca_parse(path):
    return {
        "boxes": otseca_box_counts(path),
        "general": otseca_distro_info(path)
    }
