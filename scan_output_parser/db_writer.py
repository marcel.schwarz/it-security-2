import sqlite3
import json

DB_NAME = "scans.db"


def write_run_to_db(run):
    conn = sqlite3.connect(DB_NAME)
    run_data = (run.id, run.platform, run.system, run.version, run.path)
    conn.execute("INSERT OR IGNORE INTO runs VALUES (?, ?, ?, ?, ?)", run_data)

    for otseca_res in run.otseca_results:
        data = (
            run.id, otseca_res.run_nr, str(otseca_res.path),
            *list(map(json.dumps, otseca_res.result['boxes'].values())),
            *list(map(json.dumps, otseca_res.result['general'].values()))
        )
        conn.execute("INSERT OR IGNORE INTO otseca_results VALUES (" + "".join("?," * 13) + "?)", data)

    for testssl_res in run.testssl_results:
        data = (
            run.id, testssl_res.run_nr, str(testssl_res.path),
            *list(map(json.dumps, testssl_res.result.values()))
        )
        conn.execute("INSERT OR IGNORE INTO testssl_results VALUES (" + "".join("?," * 11) + "?)", data)

    for lynis_res in run.lynis_results:
        categories = list(lynis_res.result.values())[:31]
        general = list(lynis_res.result.values())[31]
        data = (
            run.id, lynis_res.run_nr, str(lynis_res.path),
            *list(map(json.dumps, categories)),
            *general.values()
        )
        print(data)
        conn.execute("INSERT OR IGNORE INTO lynis_results VALUES (" + "".join("?," * 37) + "?)", data)
    conn.commit()
