import glob
import os.path
import re
from dataclasses import dataclass
from typing import List

from dataclasses_json import dataclass_json

from db_writer import write_run_to_db
from lynis import lynis_parse
from otseca import otseca_parse
from testssl import testssl_parse

BASE_SCAN_PATH = os.path.join("..", "raw_scans")


@dataclass_json
@dataclass
class Result:
    path: str
    run_nr: int
    result: dict


@dataclass_json
@dataclass
class Run:
    id: int
    platform: str
    system: str
    version: str
    path: str
    otseca_results: List[Result]
    lynis_results: List[Result]
    testssl_results: List[Result]


def main():
    list_of_all = []
    all_scans = glob.glob(os.path.join(BASE_SCAN_PATH, "*", ""))
    for scan in all_scans:
        findings = re.findall(r"(\d+)_(.*)_(.*)_(.*)", os.path.dirname(scan))
        findings = findings[0]
        list_of_all.append(
            Run(findings[0], findings[1], findings[2], findings[3], scan, [], [], [])
        )
    for run in list_of_all:
        for otseca_path in glob.glob(os.path.join(run.path, "otseca*", "report*")):
            nr = re.findall(r"otseca-(\d+)", otseca_path)[0]
            run.otseca_results.append(Result(otseca_path, nr, otseca_parse(otseca_path)))
        for log_file in os.listdir(run.path):
            path = os.path.join(run.path, log_file)
            nr = re.findall(r"(\d+)", log_file)[0]
            if "lynis-console-" in log_file:
                run.lynis_results.append(Result(path, nr, lynis_parse(path)))
            if "testssl-" in log_file:
                run.testssl_results.append(Result(path, nr, testssl_parse(path)))
    with open("bla.json", "w") as handle:
        handle.write(Run.schema().dumps(list_of_all, many=True))

    for run in list_of_all:
        write_run_to_db(run)


if __name__ == '__main__':
    main()
